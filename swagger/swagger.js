const swaggerAutogen = require("swagger-autogen")();

const doc = {
  swaggwer: "2.0.0",
  info: {
    title: "Users Module",
    description: "Endpoints for users",
  },
  host: "localhost:4444",
};

const outputFile = "./swagger/swagger_output.json";
const endpointsFiles = ["./package/user/bootstrap/user.express.routes.ts"];

swaggerAutogen(outputFile, endpointsFiles, doc);
