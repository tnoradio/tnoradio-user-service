const util = require("util");
const multer = require("multer");
const GridFsStorage = require("multer-gridfs-storage");
import config from '../config';

var storage = new GridFsStorage({
  url: config.databaseURL,
  options: { useNewUrlParser: true, useUnifiedTopology: true },
  file: (req, file) => {
    const match = ["image/png", "image/jpeg"];

    if (match.indexOf(file.mimetype) === -1) {
      const filename = `${file.originalname}`;
      return filename;
    }

    if (file.originalname !== undefined && 
        file.originalname !== null && 
        file.originalname.includes("talent"))
            return {
                bucketName: "user_talent_photos",
                filename: `${file.originalname}`
            };
    if (file.originalname !== undefined && 
        file.originalname !== null && 
        file.originalname.includes("profile"))
            return {
                bucketName: "user_profile_photos",
                filename: `${file.originalname}`
        };
  }
});

var uploadFile = multer({ storage: storage }).single("file");
var uploadFilesMiddleware = util.promisify(uploadFile);

export default uploadFilesMiddleware;