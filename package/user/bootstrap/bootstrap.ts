//BOOTSTRAP PATHS

import { UserMongoRepository } from "../core/infrastructure/mongo/user.mongo.repository";
import { UserRepository } from "../core/domain/services/user.service.repository";

import { UsersRead } from "../core/application/useCases/UserReader";
import { GetAllUsersController } from "../core/application/controller/getAllUsersController";

import { CreateUserController } from "../core/application/controller/createUserController";
import { UserCreator } from "../core/application/useCases/UserCreator";

import { UniqueValidateController } from "../core/application/controller/uniqueValidateEmailController";
import { UserVerifyEmail } from "../core/application/useCases/UniqueEmailValidator";

import { SignInController } from "../core/application/controller/auth/signInController";
import { SignIn } from "../core/application/useCases/auth/SignIn";

import { UpdateUserController } from "../core/application/controller/updateUserController";
import { UserUpdate } from "../core/application/useCases/UserUpdate";

import { GetUserByEmailController } from "../core/application/controller/getUserByEmailController";
import { UserByEmailGetter } from "../core/application/useCases/UserByEmailGetter";

import { GetUserByIdController } from "../core/application/controller/getUserByIdController";
import { UserByIdGetter } from "../core/application/useCases/UserByIdGetter";

import { DestroyUserController } from "../core/application/controller/destroyUserController";
import { UserDestory } from "../core/application/useCases/UserDestroy";

import { DeleteUserController } from "../core/application/controller/deleteUserController";
import { RoleCreator } from "../core/application/useCases/RoleCreator";

import { CreateRoleController } from "../core/application/controller/createRoleController";
import { UserDelete } from "../core/application/useCases/UserDeleted";

import { GetUserImageController } from "../core/application/controller/getUserImageController";
import { ImageGetter } from "../core/application/useCases/ImageGetter";

import { SignUpController } from "../core/application/controller/auth/signUpController";
import { SignUp } from "../core/application/useCases/auth/SignUp";

import { CreateNotificationController } from "../core/application/controller/createNotificationController";
import { NotificationCreator } from "../core/application/useCases/NotificationCreator";

import { HealthController } from "../core/application/controller/health.controller";
import { UsersHealthGetter } from "../core/application/useCases/UsersHealthGetter.uc";
import { UserImagesLocalRepository } from "../core/infrastructure/userImages.local.repository";
import { UserImageRepository } from "../core/domain/services/user.images.repository";
import { UploadImagesController } from "../core/application/controller/uploadImages.controller";
import { UploadImage } from "../core/application/useCases/UploadImage";
import { UserBySlugGetter } from "../core/application/useCases/UserBySlugGetter";
import { GetUserBySlugController } from "../core/application/controller/getUserBySlugController";

import { UploadImageToDBController } from "../core/application/controller/uploadImageToDBController";
import { UploadImageToDatabase } from "../core/application/useCases/UploadImageToBD.uc";

import { UserImageDBGetter } from "../core/application/useCases/UserImageDBGetter.uc";
import { GetImageFromDBController } from "../core/application/controller/getUserImageFromDBController";

import { UpdateImageInDB } from "../core/application/useCases/UpdateImageInDB.uc";
import { UpdateImageInDBController } from "../core/application/controller/updateImageInDBController";

import { MailSender } from "../core/application/useCases/MailSender";
import { SendEmailController } from "../core/application/controller/sendEmailController";
import { UserEmailService } from "../core/domain/services/user.service.email";
import { NodemailerEmailSender } from "../core/infrastructure/emailManagement/nodemailer.email.sender";

import { EmployeeTurnsReader } from "../core/application/useCases/EmployeeTurnsReader";
import { GetEmployeeTurnsByIdController } from "../core/application/controller/getEmployeeTurnsByIdController";

import { EmployeeTurnsUpdater } from "../core/application/useCases/EmployeeTurnsUpdater";
import { UpdateEmployeeTurnsController } from "../core/application/controller/updateEmployeeTurnsController";

import { EmployeeTurnCreator } from "../core/application/useCases/EmployeeTurnCreator";
import { CreateEmployeeTurnController } from "../core/application/controller/createEmployeeTurnController";

import { EmployeeTurnDestroyer } from "../core/application/useCases/EmployeeTurnDestroyer";
import { DestroyEmployeeTurnController } from "../core/application/controller/destroyEmployeeTurnController";

import { NotificationRead } from "../core/application/useCases/NotificationReader";
import { GetNotificationByIdController } from "../core/application/controller/getNotificationByIdController";

import { NotificationUpdateStatus } from "../core/application/useCases/NotificationUpdateStatus";
import { UpdateNotificationStatusController } from "../core/application/controller/updateNotificationStatusController";
import { PasswordUpdater } from "../core/application/useCases/PasswordUpdater";
import { UpdatePasswordController } from "../core/application/controller/resetPasswordController";

// BOOTSTRAP SERVICES
/**
 * Aquí se decide cuál implementación de las
 * diferentes infraestructuras se va a utilizar.
 */
const userRepository: UserRepository = new UserMongoRepository();
const userImageRepository: UserImageRepository =
  new UserImagesLocalRepository();
const emailService: UserEmailService = new NodemailerEmailSender();

// BOOTSTRAP COMMANDS  (USER CASE)
const getAllUsers = new UsersRead(userRepository);
const createUser = new UserCreator(userRepository);
const getVerifyEmail = new UserVerifyEmail(userRepository);
const getUserLogin = new SignIn(userRepository);
const getUserByEmail = new UserByEmailGetter(userRepository);
const getUserById = new UserByIdGetter(userRepository);
const getUserBySlug = new UserBySlugGetter(userRepository);
const updateUser = new UserUpdate(userRepository);
const destroyUser = new UserDestory(userRepository);
const deleteUser = new UserDelete(userRepository);
const signup = new SignUp(userRepository);
const createRole = new RoleCreator(userRepository);
const usersHealthGetter = new UsersHealthGetter();
const getUserImage = new ImageGetter(userImageRepository);
const uploadImage = new UploadImage();
const uploadImageToDb = new UploadImageToDatabase(userRepository);
const imageFromDbGetter = new UserImageDBGetter(userRepository);
const updateImageInDB = new UpdateImageInDB(userRepository);
const sendEmail = new MailSender(emailService);
const createNotification = new NotificationCreator(userRepository);
const getNotificationsById = new NotificationRead(userRepository);
const updateNotificationStatus = new NotificationUpdateStatus(userRepository);
const createEmployeeTurn = new EmployeeTurnCreator(userRepository);
const getEmployeeTurnsById = new EmployeeTurnsReader(userRepository);
const employeeTurnsUpdater = new EmployeeTurnsUpdater(userRepository);
const employeeTurnsDestroyer = new EmployeeTurnDestroyer(userRepository);
const passwordResetter = new PasswordUpdater(userRepository);

// BOOTSTRAP CONTROLLERS
const createEmployeeTurnController = new CreateEmployeeTurnController({
  createEmployeeTurn,
});
const getEmployeeTurnsByIdController = new GetEmployeeTurnsByIdController({
  getEmployeeTurnsById,
});
const updateEmployeeTurnsController = new UpdateEmployeeTurnsController({
  employeeTurnsUpdater,
});

const destroyEmployeeTurnsController = new DestroyEmployeeTurnController({
  employeeTurnsDestroyer,
});
const getAllUsersController = new GetAllUsersController({ getAllUsers });
const createUserController = new CreateUserController({ createUser });
const getValidateEmailController = new UniqueValidateController({
  getVerifyEmail,
});
const getUserLoginController = new SignInController({ getUserLogin });
const getUserByEmailController = new GetUserByEmailController({
  getUserByEmail,
});
const getUserByIdController = new GetUserByIdController({ getUserById });
const getUserBySlugController = new GetUserBySlugController({ getUserBySlug });
const updateUserController = new UpdateUserController({ updateUser });
const destroyUserController = new DestroyUserController({ destroyUser });
const deleteUserController = new DeleteUserController({ deleteUser });
const signUpController = new SignUpController({ signup });
const createRoleController = new CreateRoleController({ createRole });
const usersHealthController = new HealthController({ usersHealthGetter });
const userImageController = new GetUserImageController({ getUserImage });
const uploadImagesController = new UploadImagesController({ uploadImage });
const uploadImageToDbController = new UploadImageToDBController({
  uploadImageToDb,
});
const getImageFromDBController = new GetImageFromDBController({
  imageFromDbGetter,
});
const updateImageInDBController = new UpdateImageInDBController({
  updateImageInDB,
});

const emailSenderController = new SendEmailController({ sendEmail });
const createNotificationController = new CreateNotificationController({
  createNotification,
});
const getNotificationByIdController = new GetNotificationByIdController({
  getNotificationsById,
});
const updateNotificationStatusController =
  new UpdateNotificationStatusController({ updateNotificationStatus });

const updatePasswordController = new UpdatePasswordController({
  passwordResetter,
});

export {
  getAllUsersController,
  createUserController,
  getValidateEmailController,
  getUserLoginController,
  getUserByEmailController,
  getUserByIdController,
  updateUserController,
  destroyUserController,
  deleteUserController,
  signUpController,
  createRoleController,
  usersHealthController,
  userImageController,
  uploadImagesController,
  getUserBySlugController,
  uploadImageToDbController,
  getImageFromDBController,
  updateImageInDBController,
  emailSenderController,
  createNotificationController,
  getNotificationByIdController,
  updateNotificationStatusController,
  createEmployeeTurnController,
  getEmployeeTurnsByIdController,
  updateEmployeeTurnsController,
  destroyEmployeeTurnsController,
  updatePasswordController,
};
