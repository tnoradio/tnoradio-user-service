/**
 * Routes to User Service.
 */
import express from "express";
import upload from "../middlewares/multer";
import config from "../config";

import {
  getAllUsersController,
  createUserController,
  getValidateEmailController,
  getUserLoginController,
  getUserByEmailController,
  getUserByIdController,
  updateUserController,
  destroyUserController,
  deleteUserController,
  signUpController,
  usersHealthController,
  userImageController,
  uploadImagesController,
  getUserBySlugController,
  getImageFromDBController,
  uploadImageToDbController,
  updateImageInDBController,
  emailSenderController,
  createNotificationController,
  getNotificationByIdController,
  updateNotificationStatusController,
  getEmployeeTurnsByIdController,
  updateEmployeeTurnsController,
  createEmployeeTurnController,
  destroyEmployeeTurnsController,
  updatePasswordController,
} from "./bootstrap";

var router = express.Router();

router.get(
  /**
   * #swagger.description = 'Return response if service is up'
   * #swagger.produces = ['text']
   * #swagger.response[200] = '🛡️  Users is ok on port: 4444 🛡️'
   *
   */
  "/api/users/health",
  usersHealthController.handle.bind(usersHealthController)
);

router.get(
  /**
   * #swagger.description = 'Return List of Users'
   * #swagger.produces = ['application/json']
   * #swagger.response[200] = '🛡️  Users is ok on port: 4444 🛡️'
   *
   */
  "/api/users/index",
  getAllUsersController.handle.bind(getAllUsersController)
);

router.get(
  "/api/users/verifyemail",
  getValidateEmailController.handle.bind(getValidateEmailController)
);
router.get(
  "/api/users/getuserbyemail/:_email",
  getUserByEmailController.handle.bind(getUserByEmailController)
);
router.get(
  "/api/users/getuserbyid/:_id",
  getUserByIdController.handle.bind(getUserByIdController)
);
router.get(
  "/api/users/notifications/getbyuserid/:_id",
  getNotificationByIdController.handle.bind(getNotificationByIdController)
);
router.get(
  "/api/users/employeeturns/:_id",
  getEmployeeTurnsByIdController.handle.bind(getEmployeeTurnsByIdController)
);
router.get(
  "/api/users/details/:slug",
  getUserBySlugController.handle.bind(getUserBySlugController)
);
router.post(
  "/api/users/save",
  createUserController.handle.bind(createUserController)
);
router.post(
  "/api/users/notifications/save",
  createNotificationController.handle.bind(createNotificationController)
);
router.post(
  "/api/users/employeeturn/save",
  createEmployeeTurnController.handle.bind(createEmployeeTurnController)
);
router.post(
  "/api/users/login",
  getUserLoginController.handle.bind(getUserLoginController)
);
router.post(
  "/api/users/signup",
  signUpController.handle.bind(signUpController)
);
router.patch(
  "/api/users/update/:_id",
  updateUserController.handle.bind(updateUserController)
);
router.patch(
  "/api/users/delete/:_id",
  deleteUserController.handle.bind(deleteUserController)
);
router.patch(
  "/api/users/updateimageindb",
  upload.single("image"),
  updateImageInDBController.handle.bind(updateImageInDBController)
);
router.patch(
  "/api/users/notifications/updatestatus/:_id",
  updateNotificationStatusController.handle.bind(
    updateNotificationStatusController
  )
);
router.patch(
  "/api/users/employeeturn/update/:_id",
  updateEmployeeTurnsController.handle.bind(updateEmployeeTurnsController)
);
router.delete(
  "/api/users/destroy/:_id",
  destroyUserController.handle.bind(destroyUserController)
);
router.delete(
  "/api/users/employeeturn/destroy/:_id",
  destroyEmployeeTurnsController.handle.bind(destroyEmployeeTurnsController)
);
router.get(
  "/api/users/image/:type/:name",
  userImageController.handle.bind(userImageController)
);
router.get(
  "/api/users/sendemail/:email/:subject/:text",
  emailSenderController.handle.bind(emailSenderController)
);
router.get(
  "/api/users/imagefromdb/:name/:slug",
  getImageFromDBController.handle.bind(getImageFromDBController)
);
router.patch(
  "/api/users/resetpassword",
  updatePasswordController.handle.bind(updatePasswordController)
);
//router.post("/api/users/upload", upload.single('upload'), uploadImagesController.handle.bind(uploadImagesController));
router.post(
  "/api/users/upload",
  uploadImagesController.handle.bind(uploadImagesController)
);
router.post(
  "/api/users/uploadtodb",
  upload.single("image"),
  uploadImageToDbController.handle.bind(uploadImageToDbController)
);

export default router;
