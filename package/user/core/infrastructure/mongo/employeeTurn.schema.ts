import mongoose from "mongoose";
import { IEmployeeTurn } from "../../domain/Interfaces/IEmployeeTurn";
import employeeActivitySchema from "./employeeActivity.schema";

//import uniqueValidator from "mongoose-unique-validator";
const Schema = mongoose.Schema;

const EmployeeTurnSchema = new Schema(
  {
    startTime: {
      type: Date,
    },
    endTime: {
      type: Date,
    },
    date: {
      type: Date,
    },
    owner: {
      type: Schema.Types.ObjectId,
      ref: "user",
    },
    activities: {
      type: [employeeActivitySchema.schema]
    }
  },
  {
    timestamps: true,
  }
);

//User constant represents the entire collection of data
export default mongoose.model<IEmployeeTurn & mongoose.Document>(
  "employeeTurn",
  EmployeeTurnSchema
);
