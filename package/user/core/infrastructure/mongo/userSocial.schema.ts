import mongoose from "mongoose";
import { IUserSocial } from "../../domain/Interfaces/IUserSocial"
//import uniqueValidator from "mongoose-unique-validator";
const Schema = mongoose.Schema;

const UserSocialSchema = new Schema({
    socialNetwork: {
        type: String,
        default: '',
        required: [true]
    },
    url: {
        type: String,
        default: '',
        required: [true]
    },
    userName: {
        type: String,
        default: '',
        required: [true]
    }
}, 
{
    timestamps: true,
});

/*UserSchema.plugin(uniqueValidator, {
    message: '{PATH} debe de ser único'
})*/

//User constant represents the entire collection of data
export default mongoose.model<IUserSocial & mongoose.Document>("userSocial", UserSocialSchema);