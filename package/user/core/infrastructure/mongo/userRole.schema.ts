import mongoose from "mongoose";
import { IUserRole } from "../../domain/Interfaces/IUserRole"
//import uniqueValidator from "mongoose-unique-validator";
const Schema = mongoose.Schema;

let validRoles = {
    values: [
      "ADMIN",
      "EMPLOYEE",
      "HOST",
      "PRODUCER",
      "WRITER",
      "AUDIENCE",
      "CLIENT",
      "POTENTIAL_CLIENT",
      "INTERN",
      "EMPLOYEE_ADMIN",
      "SUPER_ADMIN",
      "INSTRUCTOR",
      "SUPER_ADMIN",
      "PRODUCTION_ASSISTANT",
      "PRODUCTION_MANAGER",
      "PRODUCTION_OPERATOR",
      "GUEST",
      "CUSTOMER",
  ],
    message: "{VALUE} it is not a valid role",
  };

const UserRoleSchema = new Schema({
    role: {
        type: String,
        default: 'AUDIENCE',
        required: [true],
        enum: validRoles,
    }
}, 
{
    timestamps: true,
});

/*UserSchema.plugin(uniqueValidator, {
    message: '{PATH} debe de ser único'
})*/

//User constant represents the entire collection of data
export default mongoose.model<IUserRole & mongoose.Document>("userRole", UserRoleSchema);