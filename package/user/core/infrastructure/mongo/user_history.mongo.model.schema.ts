import mongoose from "mongoose";
import { IUserHistory } from "../../domain/Interfaces/IUserHistory";
//import uniqueValidator from "mongoose-unique-validator";
const Schema = mongoose.Schema;

let validRoles = {
  values: [
      "EMPLOYEE",
      "HOST",
      "PRODUCER",
      "USER",
      "AUDIENCE",
      "CLIENT",
      "POTENTIAL_CLIENT",
      "INTERN",
      "EMPLOYEE_ADMIN",
      "SUPER_ADMIN",
      "INSTRUCTOR",
      "SUPER_ADMIN",
      "PRODUCTION_ASSISTANT",
      "PRODUCTION_MANAGER",
      "PRODUCTION_OPERATOR",
      "HOST",
      "GUEST",
      "CUSTOMER",
  ],
  message: "{VALUE} it is not a valid role",
};

const UserHistorySchema = new Schema(
  {
    user_id: {
      type: String,
    },
    name: {
      type: String,
    },
    lastName: {
      type: String,
    },
    email: {
      value: String,
    },
    dni: {
      type: Number,
    },
    password: {
      type: String,
      //required: "Password is required!",
    },
    isActive: {
      type: Boolean,
      default: false,
    },
    isWorking: {
      type: Boolean,
      default: false,
    },
    isStarred: {
      type: Boolean,
      default: false,
    },
    gender: {
      type: String,
    },
    role: {
      type: String,
      default: "USER",
      required: [true],
      enum: validRoles,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
    updatedBy: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

//removes the password from the query when the user returns
UserHistorySchema.methods.toJSON = function () {
  let user = this;
  let userObject = user.toObject();
  return userObject;
};

/*UserSchema.plugin(uniqueValidator, {
    message: '{PATH} debe de ser único'
})*/

//User constant represents the entire collection of data
export default mongoose.model<IUserHistory & mongoose.Document>(
  "user_history",
  UserHistorySchema
);
