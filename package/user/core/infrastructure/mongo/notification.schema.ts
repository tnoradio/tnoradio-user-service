import mongoose from "mongoose";
import { INotification } from "../../domain/Interfaces/INotification";

//import uniqueValidator from "mongoose-unique-validator";
const Schema = mongoose.Schema;

const NotificationSchema = new Schema({
    type: {
        type: String,
    },
    message: {
        type: String,
    },
    isActive: {
        type: Boolean,
    },
    user: {
        type: Schema.Types.ObjectId, 
        ref: 'user'
    }
}, 
{
    timestamps: true,
});


//User constant represents the entire collection of data
export default mongoose.model<INotification & mongoose.Document>("notification", NotificationSchema);