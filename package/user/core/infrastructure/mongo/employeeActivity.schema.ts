import mongoose from "mongoose";
import { IEmployeeActivity } from "../../domain/Interfaces/IEmployeeActivity";

const Schema = mongoose.Schema;

const EmployeeActivitySchema = new Schema(
  {
    duration: {
      type: Number,
    },
    name: {
      type: String,
    },
    taskId: {
      type: String,
    },
    turnId: {
      type: Schema.Types.ObjectId,
      ref: "employeeTurn",
    },
  },
  {
    timestamps: true,
  }
);

//User constant represents the entire collection of data
export default mongoose.model<IEmployeeActivity & mongoose.Document>(
  "employeeActivity",
  EmployeeActivitySchema
);
