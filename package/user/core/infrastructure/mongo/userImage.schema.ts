import mongoose from "mongoose";
import { IUserImage } from "../../domain/Interfaces/IUserImage"
//import uniqueValidator from "mongoose-unique-validator";
const Schema = mongoose.Schema;

const UserImageSchema = new Schema({
    imageName: {
        type: String,
        default: '',
        required: [true]
    },
    imageUrl: {
        type: String,
        default: '',
        required: [true]
    },
    file:
    {
        data: Buffer,
        contentType: String
    },
    owner: {
        type: Schema.Types.ObjectId,
        ref: "User"
    }
}, 
{
    timestamps: true,
});

/*UserSchema.plugin(uniqueValidator, {
    message: '{PATH} debe de ser único'
})*/

//User constant represents the entire collection of data
export default mongoose.model<IUserImage & mongoose.Document>("userImage", UserImageSchema);