import mongoose from "mongoose";
import { IUser } from "../../domain/Interfaces/IUser";
//import UserImageSchema from "./userImage.schema";
import UserRoleSchema from "./userRole.schema";
import UserSocialSchema from "./userSocial.schema";
import EmployeeTurnSchema from "./employeeTurn.schema";
//import uniqueValidator from "mongoose-unique-validator";
const Schema = mongoose.Schema;

let validGenders = {
  values: ["F", "M", "O"],
  message: "{VALUE} it is not a valid gender",
};

const UserSchema = new Schema(
  {
    name: {
      type: String,
    },
    lastName: {
      type: String,
    },
    bio: {
      type: String,
    },
    email: {
      value: String,
    },
    DNI: {
      type: Number,
    },
    password: {
      type: String,
      required: "Password is required!",
    },
    isActive: {
      type: Boolean,
      default: true,
    },
    isWorking: {
      type: Boolean,
      default: false,
    },
    gender: {
      type: String,
      enum: validGenders,
    },
    images: [
      {
        type: Schema.Types.ObjectId,
        ref: "UserImage",
      },
    ],
    roles: {
      type: [UserRoleSchema.schema],
    },
    socials: {
      type: [UserSocialSchema.schema],
    },
    turns: {
      type: [EmployeeTurnSchema.schema],
    },
    birthdate: {
      type: Date,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
    updatedBy: {
      type: String,
    },
    company: {
      type: String,
    },
    phone: {
      type: String,
    },
    address: {
      type: String,
    },
    image: {
      type: String,
    },
    department: {
      type: String,
    },
    notes: {
      type: String,
    },
    slug: {
      type: String,
    },
    isStarred: {
      type: Boolean,
    },
  },
  {
    timestamps: true,
  }
);

//removes the password from the query when the user returns
UserSchema.methods.toJSON = function () {
  let user = this;
  let userObject = user.toObject();
  delete userObject.password;
  return userObject;
};

/*UserSchema.plugin(uniqueValidator, {
    message: '{PATH} debe de ser único'
})*/

//User constant represents the entire collection of data
export default mongoose.model<IUser & mongoose.Document>("user", UserSchema);
