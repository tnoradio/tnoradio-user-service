import User from "./user.schema";
import Notification from "./notification.schema";
import UserHistory from "./user_history.mongo.model.schema";
import UserEmail from "../../domain/entities/UserEmail";
import DomainUser from "../../domain/entities/User";
import bcrypt from "bcrypt";
import { UserRepository } from "../../domain/services/user.service.repository";
import jwtoken from "../../../utils/jwt";
import { UserSession } from "../../domain/entities/UserSession";
import _ from "lodash";
import UserRole from "../../domain/entities/UserRole";
import UserRoleSchema from "./userRole.schema";
import UserImage from "../../domain/entities/UserImage";
import Image from "./userImage.schema";
import DomainNotification from "../../domain/entities/Notification";
import EmployeeTurn from "../../domain/entities/EmployeeTurn";
import EmployeeTurnSchema from "./employeeTurn.schema";

export class UserMongoRepository implements UserRepository {
  async resetPassword(_id: String, password: String): Promise<any> {
    try {
      console.log("reset password");
      console.log("This is ID: ", _id, "This is PASS: ", password);
      const user = await User.findById(_id);
      console.log(user);
      user.password = bcrypt.hashSync(password, 7);
      const result = await User.findOneAndUpdate({ _id: _id }, user).catch(
        (err) => {
          throw err;
        }
      );
      console.log(result);
      return result;
      /*if (bcrypt.compareSync(oldPassword, user.password)) {
        user.password = bcrypt.hashSync(password, 7);
        user.passwordConfirmation = bcrypt.hashSync(password, 7);
        const result = await User.findByIdAndUpdate(
          { _id: _id },
          user,
          function (err) {
            return err;
          }
        );
        return result;
      } else throw new Error("Couldn't update password");*/
    } catch (err) {
      throw err;
    }
  }
  async saveEmployeeTurn(employeeTurn: EmployeeTurn): Promise<EmployeeTurn> {
    try {
      return await EmployeeTurnSchema.create(employeeTurn);
    } catch (err) {
      console.log(err);
      return err;
    }
  }
  async getEmployeeTurns(_id: string): Promise<EmployeeTurn[]> {
    try {
      return await EmployeeTurnSchema.find({ owner: _id });
    } catch (err) {
      throw err;
    }
  }
  async updateEmployeeTurn(updateValues: any): Promise<EmployeeTurn> {
    try {
      const turnToUpdate = await EmployeeTurnSchema.findById(updateValues._id);
      const turnToUpdateCreated = this.createTurnToUpdate(
        turnToUpdate,
        updateValues
      );

      const result = await EmployeeTurnSchema.findByIdAndUpdate(
        { _id: updateValues._id },
        turnToUpdateCreated
      );
      console.log(result);
      return turnToUpdateCreated;
    } catch (err) {
      let errorObject = JSON.parse(JSON.stringify(err));
      throw err;
    }
  }

  async signup(user: DomainUser): Promise<any | Error> {
    try {
      user.password = bcrypt.hashSync(user.password, 7);
      let userSignUp = await User.create(user);
      return await jwtoken.generateToken(userSignUp);
    } catch (err) {
      return err;
    }
  }

  async update(user: DomainUser, id: string): Promise<Error | DomainUser> {
    try {
      const res = await User.findById(id);
      console.log("This is my user", res);
      const result = await User.findByIdAndUpdate(
        { _id: id },
        user,
        { runValidators: true },
        function (err) {
          console.log("ERR IN UPDATE ", err);
          return err;
        }
      );
      return user;
    } catch (err) {
      let errorObject = JSON.parse(JSON.stringify(err));
      console.log("ERR IN CATCH ", errorObject);
      throw err;
    }
  }

  /**
   * Function changes user status to removed setting
   * isDeleted field true
   * @param userId ID from user
   */
  async delete(_id: String): Promise<DomainUser | Error> {
    try {
      const deletedUser = await User.findByIdAndUpdate(
        { _id: _id },
        { isDeleted: true }
      );
      return deletedUser;
    } catch (err) {
      return null;
    }
  }

  /**
   * Insert from table user_history
   * @param user Domain User
   */
  async insertFromHistoryUser(user: DomainUser): Promise<any | Error> {
    try {
      // Instance Model Mongoose
      const mUser: typeof User.prototype = user;

      // .toObject() function transforms a document from mongoose to a simple  javascript object.
      // @TODOS read documentation https://mongoosejs.com/docs/guide.html#toObject
      const userUpdated = mUser.toObject();

      // Save id user in another attribute other than _id
      Reflect.set(userUpdated, "user_id", userUpdated._id);
      //Delete property _id front object
      Reflect.deleteProperty(userUpdated, "_id");

      await UserHistory.create(userUpdated);
    } catch (err) {
      console.log(err);
      return err;
    }
  }

  async save(user: DomainUser): Promise<DomainUser> {
    try {
      user.password = bcrypt.hashSync(user.password, 7);

      return await User.create(user);
    } catch (err) {
      console.log(err);
      return err;
    }
  }

  async saveRole(role: UserRole): Promise<UserRole> {
    try {
      return await UserRoleSchema.create(role);
    } catch (err) {
      return err;
    }
  }

  async isValidEmail(email: UserEmail): Promise<Boolean> {
    try {
      let isTaken = await User.findOne({
        email: email,
      }).where("isDeleted === false");

      if (isTaken === null) {
        return false;
      } else return true;
    } catch (err) {
      return err;
    }
  }

  async getAll(): Promise<Error | DomainUser[]> {
    try {
      return User.find();
    } catch (err) {
      return err;
    }
  }

  async getUserByEmail(email: UserEmail): Promise<DomainUser> {
    try {
      const user = await User.findOne({ email: email });
      if (user == null) return null;
      else return user;
    } catch (err) {
      return err;
    }
  }

  async getUserById(_id: String): Promise<DomainUser> {
    console.log("mongo getUserById _id", _id);
    try {
      const user = await User.findOne({ _id: _id });
      console.log("mongo getUserById", user);
      if (user == null) return null;
      else return user;
    } catch (err) {
      return err;
    }
  }

  async getUserBySlug(_slug: String): Promise<DomainUser> {
    try {
      const user = await User.findOne({ slug: _slug });
      if (user == null) return null;
      else return user;
    } catch (err) {
      return err;
    }
  }

  async getUserImage(name: String, slug: String): Promise<UserImage> {
    try {
      const image = await Image.findOne({ imageName: name, imageUrl: slug });
      if (image == null) return null;
      else return image;
    } catch (err) {
      return err;
    }
  }

  async saveUserImage(image): Promise<UserImage> {
    try {
      const res = await Image.create(image);
      if (image == null) return null;
      else return res;
    } catch (err) {
      return err;
    }
  }

  async updateUserImage(image, slug, name): Promise<UserImage> {
    let createResponse;
    const updateResponse = await Image.findOneAndUpdate(
      { imageName: name, imageUrl: slug },
      image
    );

    if (updateResponse === null) {
      createResponse = await Image.create(image);
      return createResponse;
    } else return updateResponse;
  }

  async login(email: UserEmail, password: String): Promise<Error | any> {
    try {
      const user = await User.findOne({ email: email });

      if (!user) {
        throw new Error("Invalid email!");
      }
      if (!bcrypt.compareSync(password, user.password)) {
        throw new Error("Invalid email or password");
      }

      return jwtoken.generateToken(user);
    } catch (err) {
      console.log("ERROR MONN", err.message);
      return err;
    }
  }

  async destroy(userId: String): Promise<any | Error> {
    try {
      //console.log(userId);

      User.findByIdAndRemove({ _id: userId })
        .then((res) => {
          console.log("Res user");
          console.log(res);
          Image.findOneAndRemove({ owner: userId, imageName: "profile" })
            .then((res) => {
              console.log("Res prfile");
              console.log(res);
              Image.findOneAndRemove({ owner: userId, imageName: "talent" })
                .then((res) => {
                  console.log("Res talent");
                  console.log(res);
                  return res;
                })
                .catch((error) => {
                  console.log("Error deleting talent image " + error);
                  return error;
                });
            })
            .catch((error) => {
              console.log("Error deleting profile image " + error);
              return error;
            })
            .catch((error) => {
              console.log(error);
              return error;
            });
        })
        .catch((error) => {
          console.log("Error deleting user " + error);
          return error;
        });
    } catch (err) {
      return err;
    }
  }

  async destroyEmployeeTurn(employeeTurnId: String): Promise<any> {
    try {
      EmployeeTurnSchema.findByIdAndRemove({ _id: employeeTurnId })
        .then((res) => {
          console.log("Res employeeTurn");
          console.log(res);
          return res;
        })
        .catch((error) => {
          console.log("Error deleting user " + error);
          return error;
        });
    } catch (err) {
      return err;
    }
  }

  async destroyImage(imageUrl: String): Promise<any> {
    try {
      return await Image.findOneAndDelete({ imageUrl: imageUrl });
    } catch (err) {
      return err;
    }
  }

  //TODO: Verificar que en efecto esto se enera y elimina con cada
  //sesión o si no queda permanente.
  async logout(userSession: UserSession): Promise<Error | Boolean> {
    try {
      return true;
    } catch (err) {
      return err;
    }
  }

  /**
   *
   * NOTIFICACIONES
   *
   */
  async saveNotification(
    notification: DomainNotification
  ): Promise<DomainNotification> {
    try {
      return await Notification.create(notification);
    } catch (err) {
      console.log(err);
      return err;
    }
  }

  async getAllNotificationUser(id): Promise<Error | DomainNotification[]> {
    try {
      return await Notification.find({ user: id });
    } catch (err) {
      return err;
    }
  }

  async updateStatusNotification(
    id,
    status
  ): Promise<Error | DomainNotification> {
    console.log(status);
    try {
      var filter = { _id: id };
      var update = { isActive: status };
      const res = await Notification.findOneAndUpdate(filter, update);
      console.log(res);
      return Notification.findOne(filter);
    } catch (err) {
      return err;
    }
  }

  createTurnToUpdate(turnToUpdate, updateValues) {
    const turn = {
      _id: updateValues._id,
      startTime:
        updateValues.field === "startTime"
          ? updateValues.value
          : turnToUpdate.startTime,
      endTime:
        updateValues.field === "endTime"
          ? updateValues.value
          : turnToUpdate.endTime,
      date:
        updateValues.field === "date" ? updateValues.value : turnToUpdate.date,
      owner:
        updateValues.field === "owner"
          ? updateValues.value
          : turnToUpdate.owner,
    };
    return turn;
  }
}
