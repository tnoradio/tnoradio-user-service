import { UserImageRepository } from "../domain/services/user.images.repository";

export class UserImagesLocalRepository implements UserImageRepository {
    getUserImage(imageName, options, res) {
        try {

            return res.sendFile(imageName, options, function (err) {
                if (err) {
                console.log(err);
                } else {
                console.log('Sent:', imageName)
                }
            })
        } catch (e) {
            console.log(e);
        }        
      
    }
}

