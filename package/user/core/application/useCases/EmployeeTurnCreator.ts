import EmployeeTurn from "../../domain/entities/EmployeeTurn";
import Command from "../command";
import { UserRepository } from "../../domain/services/user.service.repository";

export class EmployeeTurnCreator extends Command {
  private _repository: UserRepository;
  private _employeeTurn: EmployeeTurn;

  constructor(_repository: UserRepository) {
    super();
    this._repository = _repository;
  }

  public setEmployeeTurn(employeeTurn: EmployeeTurn) {
    this._employeeTurn = employeeTurn;
  }

  public getEmployeeTurn() {
    return this._employeeTurn;
  }

  //  Override Method
  public async execute() {
    const response = await this._repository.saveEmployeeTurn(
      this._employeeTurn
    );
    return response;
  }
}
