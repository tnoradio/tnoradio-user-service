import UserRole from '../../domain/entities/UserRole';
import Command from '../command';
import { UserImageRepository } from '../../domain/services/user.images.repository';
import e from 'express';

export class ImageGetter extends Command {

    private _repository: UserImageRepository;
    public options: {};
    public imageName: string;
    public res: e.Response;

    constructor(_repository: UserImageRepository) {
        super();
        this._repository = _repository;
    }

    public setOptions(options) {
        this.options = options;
    }

    public setImageName(imageName) {
        this.imageName = imageName;
    }

    public setResponse(res) {
        this.res = res;
    }

    public getOptions() {
        return this.options;
    }

    public getImageName() {
        return this.imageName;
    }

    public getResponse() {
        return this.res;
    }

    //  Override Method
    public async execute() {
        const response = await this._repository.getUserImage(this.imageName, this.options, this.res);
        return response;
    }
}