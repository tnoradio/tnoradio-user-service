import { UserRepository } from "../../domain/services/user.service.repository";
import Command from "../command";

export class NotificationUpdateStatus extends Command {
  private _repository: UserRepository;
  private _id: String;
  private isActive: Boolean;

  constructor(repository: UserRepository) {
    super();
    this._repository = repository;
  }

  public setId(id: String) {
    this._id = id;
  }
  public setStatus(isActive: Boolean) {
    this.isActive = isActive;
  }

  //  Override Method
  public async execute() {
    console.log(this._id, this.isActive);
    const response = await this._repository.updateStatusNotification(
      this._id,
      this.isActive
    );
    return response;
  }
}
