import UserModel from '../../domain/entities/User';
import Command from '../command';
import { UserRepository } from '../../domain/services/user.service.repository';
import UserImage from '../../domain/entities/UserImage';


/**
 * Uploads image to database.
 */
export class UploadImageToDatabase extends Command {

    private _repository: UserRepository;
    private image: UserImage;

    constructor(_repository: UserRepository) {
        super();
        this._repository = _repository;
    }

    public SetImage(image: UserImage) {
        this.image = image;
    }

    public getImage() {
        return this.image;
    }

    //  Override Method
    public async execute() {
        const response = await this._repository.saveUserImage(this.image);
        return response;
    }


}