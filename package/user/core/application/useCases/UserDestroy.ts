import Command from "../command";
import { UserRepository } from '../../domain/services/user.service.repository';

/**
 * Destroy <> Delete
 */
export class UserDestory extends Command {
    private _repository: UserRepository;
    private _userId: String;

    constructor(_repository: UserRepository) {
        super();
        this._repository = _repository;
    }

    public setUserId(userId) {
        //console.log(userId);
        this._userId = userId;
    }

    public getUserId() {
        return this._userId;
    }

    public async execute() {
        const response = await this._repository.destroy(this.getUserId());
        return response;
    }
}
