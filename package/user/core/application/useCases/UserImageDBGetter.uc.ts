import { UserRepository } from '../../domain/services/user.service.repository';
import Command from '../command';



/**
 * Gets user image from database.
 */
export class UserImageDBGetter extends Command {

    private _repository: UserRepository;
    private name: String;
    private slug: String;

    constructor(repository: UserRepository) {
        super();
        this._repository = repository;
    }

    public setImageName(name: String) {
        this.name = name;
    }

    public setImageSlug(slug: String) {
        this.slug = slug;
    }

    public getName() {
        return this.name;
    }

    public getSlug() {
        return this.slug;
    }
    //  Override Method
    async execute() {
        const response = await this._repository.getUserImage(this.getName(), this.getSlug());
        return response;
    }
}