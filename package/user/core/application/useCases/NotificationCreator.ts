import Notification from '../../domain/entities/Notification';
import Command from '../command';
import { UserRepository } from '../../domain/services/user.service.repository';


export class NotificationCreator extends Command {

    private _repository: UserRepository;
    private _notification: Notification;

    constructor(_repository: UserRepository) {
        super();
        this._repository = _repository;
    }

    public setNotificationCreate(notification: Notification) {
        this._notification = notification;
    }

    public getNotificationCreate() {
        return this._notification;
    }


    //  Override Method
    public async execute() {
        const response = await this._repository.saveNotification(this._notification);
        return response;
    }
}