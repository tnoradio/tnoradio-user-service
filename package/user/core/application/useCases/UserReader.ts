import Command from '../command';
import { UserRepository } from '../../domain/services/user.service.repository';

export class UsersRead extends Command {

    private _repository: UserRepository;
    private _typeReading: String;

    constructor(_repository: UserRepository) {
        super();
        this._repository = _repository;
    }

    public setTypeReading(type: String) {
        this._typeReading = type;
    }

    public getTypeReading() {
        return this._typeReading;
    }

    //  Override Method
    async execute() {
        if (this._typeReading == 'byEmail') {

        }
        else {
            const response = await this._repository.getAll();
            return response;
        }
    }


}