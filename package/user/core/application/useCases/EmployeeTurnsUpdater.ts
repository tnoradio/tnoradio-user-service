import { UserRepository } from "../../domain/services/user.service.repository";
import Command from "../command";

export class EmployeeTurnsUpdater extends Command {
  private _repository: UserRepository;
  private updateValues;

  constructor(repository: UserRepository) {
    super();
    this._repository = repository;
  }

  public setUpdateValues(updateValues) {
    this.updateValues = updateValues;
  }

  //  Override Method
  public async execute() {
    const response = await this._repository.updateEmployeeTurn(
      this.updateValues
    );
    return response;
  }
}
