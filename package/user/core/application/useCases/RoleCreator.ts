import UserRole from '../../domain/entities/UserRole';
import Command from '../command';
import { UserRepository } from '../../domain/services/user.service.repository';

export class RoleCreator extends Command {

    private _repository: UserRepository;
    private _role: UserRole;

    constructor(_repository: UserRepository) {
        super();
        this._repository = _repository;
    }

    public setRole(role: UserRole) {
        this._role = role;
    }

    public getRole() {
        return this._role;
    }

    //  Override Method
    public async execute() {
        const response = await this._repository.saveRole(this._role);
        return response;
    }
}