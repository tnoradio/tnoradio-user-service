import { UserRepository } from "../../domain/services/user.service.repository";
import Command from "../command";

export class PasswordUpdater extends Command {
  private _repository: UserRepository;
  private _id: String;
  private oldPassword: String;
  private password: String;

  constructor(repository: UserRepository) {
    super();
    this._repository = repository;
  }

  public setPassword(password: String) {
    this.password = password;
  }

  public setOldpassword(password: String) {
    this.oldPassword = password;
  }

  public setId(id: String) {
    this._id = id;
  }

  //  Override Method
  public async execute() {
    const response = await this._repository.resetPassword(
      this._id,
      this.password
    );
    return response;
  }
}
