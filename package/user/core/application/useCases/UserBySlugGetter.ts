import { UserRepository } from '../../domain/services/user.service.repository';
import Command from '../command';


export class UserBySlugGetter extends Command {

    private _repository: UserRepository;
    private _slug: String;

    constructor(repository: UserRepository) {
        super();
        this._repository = repository;
    }

    public createUserSlug(_slug: String) {
        this._slug = _slug;
    }

    public getSlug() {
        return this._slug;
    }
    //  Override Method
    async execute() {
        const response = await this._repository.getUserBySlug(this.getSlug());
        return response;
    }
}