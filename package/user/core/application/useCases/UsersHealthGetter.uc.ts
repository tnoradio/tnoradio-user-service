import Command from '../command';
import config from '../../../config';

export class UsersHealthGetter extends Command {

    constructor() {
        super();
    }

    //  Override Method
    public async execute() {
        const response = `🛡️  Users is ok on port: ${config.port} 🛡️`;
        console.log(response);
        return response;
    }
}