import Command from "../command";
import { UserRepository } from "../../domain/services/user.service.repository";

/**
 * Destroy <> Delete
 */
export class EmployeeTurnDestroyer extends Command {
  private _repository: UserRepository;
  private _employeeTurnId: String;

  constructor(_repository: UserRepository) {
    super();
    this._repository = _repository;
  }

  public setEmployeeTurnId(employeeTurnId) {
    //console.log(employeeTurnId);
    this._employeeTurnId = employeeTurnId;
  }

  public getEmployeeTurnId() {
    return this._employeeTurnId;
  }

  public async execute() {
    const response = await this._repository.destroyEmployeeTurn(
      this.getEmployeeTurnId()
    );
    return response;
  }
}
