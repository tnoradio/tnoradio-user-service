import UserModel from '../../domain/entities/User';
import UserEmail from '../../domain/entities/UserEmail';
import { UserRepository } from '../../domain/services/user.service.repository';
import Command from '../command';


export class UserByEmailGetter extends Command {

    private _repository: UserRepository;
    private _email: UserEmail;

    constructor(repository: UserRepository) {
        super();
        this._repository = repository;
    }

    public createUserEmail(email: UserEmail) {
        this._email = email;
    }

    public getUserEmailCreate() {
        return this._email;
    }
    //  Override Method
    async execute() {
        const response = await this._repository.getUserByEmail(this._email);
        return response;
    }
}