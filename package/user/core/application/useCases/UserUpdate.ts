import UserEmail from '../../domain/entities/UserEmail';
import { UserRepository } from '../../domain/services/user.service.repository';
import UserDomain from '../../domain/entities/User'
import Command from '../command';

export class UserUpdate extends Command {

    private _repository: UserRepository;
    private _email: UserEmail;
    private _id: String;
    private _user: UserDomain;

    constructor(repository: UserRepository) {
        super();
        this._repository = repository;
    }

    public setEmail(email: UserEmail) {
        this._email = email;
    }

    public setId(id: String) {
        this._id = id;
    }

    public setDomainUser(user: UserDomain) {
        this._user = user;
    }


    //  Override Method
    public async execute() {
        const response = await this._repository.update(this._user, this._id);
        return response;
    }


}
