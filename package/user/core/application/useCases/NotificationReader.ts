import Command from "../command";
import { UserRepository } from "../../domain/services/user.service.repository";

export class NotificationRead extends Command {
  private _repository: UserRepository;
  private _id: String;

  constructor(_repository: UserRepository) {
    super();
    this._repository = _repository;
  }

  public setId(type: String) {
    this._id = type;
  }

  public getId() {
    return this._id;
  }

  //  Override Method
  async execute() {
    const response = await this._repository.getAllNotificationUser(this._id);
    return response;
  }
}
