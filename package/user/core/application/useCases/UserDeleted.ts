import UserEmail from '../../domain/entities/UserEmail';
import { UserRepository } from '../../domain/services/user.service.repository';
import UserDomain from '../../domain/entities/User'
import Command from '../command';

export class UserDelete extends Command {

    private _repository: UserRepository;
    private _id: String;


    constructor(repository: UserRepository) {
        super();
        this._repository = repository;
    }

    public setId(id: String) {
        this._id = id;
    }

    //  Override Method
    public async execute() {
        const response = await this._repository.delete(this._id);
        return response;
    }


}
