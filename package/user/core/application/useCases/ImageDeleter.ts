import Command from "../command";
import { UserRepository } from '../../domain/services/user.service.repository';

/**
 * Destroy <> Delete
 */
export class DestroyImage extends Command {
    private _repository: UserRepository;
    private _imageUrl: String;

    constructor(_repository: UserRepository) {
        super();
        this._repository = _repository;
    }

    public setImageUrl(imageUrl) {
        //console.log(imageUrl);
        this._imageUrl = imageUrl;
    }

    public getImageUrl() {
        return this._imageUrl;
    }

    public async execute() {
        const response = await this._repository.destroy(this.getImageUrl());
        return response;
    }
}
