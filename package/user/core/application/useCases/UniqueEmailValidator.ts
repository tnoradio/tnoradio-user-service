/**
* This class acts as an interface with the 
* methods that the user CRUD must have 
*/

import Command from '../command';
import { UserRepository } from '../../domain/services/user.service.repository';
import UserEmail from '../../domain/entities/UserEmail';

export class UserVerifyEmail extends Command {
    private _repository: UserRepository;

    constructor(repository: UserRepository) {
        super();
        this._repository = repository;
    }

    private _userEmail: any;


    public setEmail(email: UserEmail) {
        this._userEmail = email;
    }

    //Override method
    public async execute() {
        const response = await this._repository.isValidEmail(this._userEmail);
        return response;
    }
}
