import { UserEmailService } from "../../domain/services/user.service.email";
import Command from "../command";

export class MailSender extends Command {
  private _sender: UserEmailService;
  private email: String;
  private text: String;
  private subject: String;

  constructor(sender: UserEmailService) {
    super();
    this._sender = sender;
  }

  public setUserEmail(email: String) {
    this.email = email;
  }

  public getUserEmail() {
    return this.email;
  }

  public setEmailText(text: String) {
    this.text = text;
  }

  public getEmailText() {
    return this.text;
  }

  public setSubject(subject: String) {
    this.subject = subject;
  }

  public getSubject() {
    return this.subject;
  }
  //  Override Method
  async execute() {
    const response = await this._sender.sendAppEmail(
      this.getUserEmail(),
      this.getSubject(),
      this.getEmailText()
    );
    return response;
  }
}
