import UserModel from '../../domain/entities/User';
import Command from '../command';
import { UserRepository } from '../../domain/services/user.service.repository';
import UserEmail from '../../domain/entities/UserEmail';

export class UserCreator extends Command {

    private _repository: UserRepository;
    private _user: UserModel;

    constructor(_repository: UserRepository) {
        super();
        this._repository = _repository;
    }

    public UserCreate(user: UserModel) {
        this._user = user;
    }

    public getUserCreate() {
        return this._user;
    }

    public async emailIsTaken(email) {
        return await this._repository.isValidEmail(email);
    }

    public validateMatchPassword(password: String, match: String) {
        if (password == match) {
            return true
        } else {
            return false
        }
    }

    //  Override Method
    public async execute() {
        const response = await this._repository.save(this._user);
        return response;
    }
}