import DomainUser from '../../../domain/entities/User';
import Command from '../../command';
import { UserRepository } from '../../../domain/services/user.service.repository';

export class SignUp extends Command {

    private _repository: UserRepository;
    private _user: DomainUser;

    constructor(_repository: UserRepository) {
        super();
        this._repository = _repository;
    }

    public UserCreate(user: DomainUser) {
        this._user = user;
    }

    public getUserCreate() {
        return this._user;
    }

    public async emailIsTaken(email) {
        return await this._repository.isValidEmail(email);
    }

    public validateMatchPassword(password: String, match: String) {
        if (password == match) {
            return true
        } else {
            return false
        }
    }

    //  Override Method
    public async execute() {
        const response = await this._repository.signup(this._user);
        return response;
    }


}