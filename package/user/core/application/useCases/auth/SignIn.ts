import UserModel from '../../../domain/entities/User';
import UserEmail from '../../../domain/entities/UserEmail';
import { UserRepository } from '../../../domain/services/user.service.repository';
import Command from '../../command';

export class SignIn extends Command {

    private _repository: UserRepository;
    private _email: UserEmail;
    private _password: String;

    constructor(repository: UserRepository) {
        super();
        this._repository = repository;
    }

    public userLoginParams(user: UserModel) {
        this._email = user.email;
        this._password = user.password;
    }

    public getEmail() {
        return this._email
    }

    public getPassword() {
        return this._password;
    }

    //  Override Method
    public async execute() {
        const response = await this._repository.login(this._email, this._password);
        return response;
    }


}
