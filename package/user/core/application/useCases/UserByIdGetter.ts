import UserModel from "../../domain/entities/User";
import UserEmail from "../../domain/entities/UserEmail";
import { UserRepository } from "../../domain/services/user.service.repository";
import Command from "../command";

export class UserByIdGetter extends Command {
  private _repository: UserRepository;
  private _id: String;

  constructor(repository: UserRepository) {
    super();
    this._repository = repository;
  }

  public createUserId(_id: String) {
    this._id = _id;
  }

  public getUserByIdCreator() {
    return this._id;
  }
  //  Override Method
  async execute() {
    const response = await this._repository.getUserById(this._id);
    return response;
  }
}
