import e from 'express';
import { UploadImage } from '../useCases/UploadImage';
import upload from '../../../middlewares/upload'


export interface UseCase {
    uploadImage: UploadImage;

}


export class UploadImagesController {

    constructor(private useCase: UseCase) { }

    async handle(req, res) {

        await upload(req, res);

        try {

            const images = await this.useCase.uploadImage.execute();
            return res.status(200).send(images);

        } catch (error) {
            console.log(error);
            return res.status(400).send(error);
        }
    }
}