import e from 'express';
import { UserDelete } from '../useCases/UserDeleted';

interface UsesCases {
    deleteUser: UserDelete;
}

export class DeleteUserController {

    constructor(private usescases: UsesCases) { }

    async handle(req: e.Request, res: e.Response) {
        console.log("CONTROLLER " , req.params);
        try {
            //Set id 
            this.usescases.deleteUser.setId(req.params._id);

            const data = await this.usescases.deleteUser.execute();
            console.log("DELETE ", data);
            if (data == null)
                return res.status(400).send("User not Deleted");
            else
                return res.status(200).send(data);
        } catch (err) {
            return res.status(400).send(err);
        }

        /*if (isDomainError(product)) {
            res.status(400).send(product.message);
        } else {
            res.send(product);
        }*/
    }
}