import e from 'express';
import { UsersRead } from '../useCases/UserReader';

interface UseCase {
    getAllUsers: UsersRead;
}

export class GetAllUsersController {
    constructor(private useCase: UseCase) { }

    async handle(req: e.Request, res: e.Response) {
        //definition to bring all users
        this.useCase.getAllUsers.setTypeReading('all');
        try {
            const users = await this.useCase.getAllUsers.execute();
            console.log("GetAllUsersController users", users);
            return res.status(200).send(users);
        } catch (err) {
            console.error(err);
            return res.status(400).send(err);
        }

        /*if (isDomainError(product)) {
            res.status(400).send(product.message);
        } else {
            res.send(product);
        }*/
    }
}