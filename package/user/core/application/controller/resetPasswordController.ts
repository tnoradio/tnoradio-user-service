import e from "express";
import { PasswordUpdater } from "../useCases/PasswordUpdater";

interface UseCase {
  passwordResetter: PasswordUpdater;
}

export class UpdatePasswordController {
  constructor(private useCase: UseCase) {}

  async handle(req, res) {
    try {
      console.info("HANDLER ", req.body);
      this.useCase.passwordResetter.setId(req.body._id);
      this.useCase.passwordResetter.setPassword(req.body.password);
      const result = await this.useCase.passwordResetter.execute();

      if (result == null) return res.status(400).send("User not Updated");
      else return res.status(200).send(result);
    } catch (err) {
      return res.status(400).send(err);
    }
  }
}
