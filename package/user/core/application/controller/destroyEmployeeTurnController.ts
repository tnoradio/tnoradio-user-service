import e from "express";
import { EmployeeTurnDestroyer } from "../useCases/EmployeeTurnDestroyer";

interface UseCase {
  employeeTurnsDestroyer: EmployeeTurnDestroyer;
}

export class DestroyEmployeeTurnController {
  constructor(private useCase: UseCase) {}

  async handle(req: e.Request, res: e.Response) {
    this.useCase.employeeTurnsDestroyer.setEmployeeTurnId(req.params._id);

    try {
      if (this.useCase.employeeTurnsDestroyer.getEmployeeTurnId != null) {
        //console.log(req.params._id);
        const data = await this.useCase.employeeTurnsDestroyer.execute();
        return res.status(200).send(data);
      } else {
        return res.status(400).send("ID not found");
      }
    } catch (err) {
      return res.status(400).send(err);
    }
  }
}
