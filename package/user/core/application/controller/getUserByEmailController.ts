import e from 'express';
import { UserByEmailGetter } from '../useCases/UserByEmailGetter';

interface UsesCases {
    getUserByEmail: UserByEmailGetter;
}

export class GetUserByEmailController {

    constructor(private usescases: UsesCases) { }

    async handle(req: e.Request, res: e.Response) {

        //definition to bring all users
        this.usescases.getUserByEmail.createUserEmail(req.body.email);

        try {
            const data = await this.usescases.getUserByEmail.execute();
            if (data == null)
                return res.status(400).send("User not Found");
            else
                return res.status(200).send(data);
        } catch (err) {
            return res.status(400).send(err);
        }

        /*if (isDomainError(product)) {
            res.status(400).send(product.message);
        } else {
            res.send(product);
        }*/
    }
}