import e from "express";
import { NotificationRead } from "../useCases/NotificationReader";

interface UseCases {
  getNotificationsById: NotificationRead;
}

export class GetNotificationByIdController {
  constructor(private usescases: UseCases) {}

  async handle(req: e.Request, res: e.Response) {
    //definition to bring all users
    this.usescases.getNotificationsById.setId(req.params._id);

    try {
      const data = await this.usescases.getNotificationsById.execute();
   
      if (data == null) return res.status(400).send("Notifications not Found");
      else return res.status(200).send(data);
    } catch (err) {
      return res.status(400).send(err);
    }

    /*if (isDomainError(product)) {
            res.status(400).send(product.message);
        } else {
            res.send(product);
        }*/
  }
}
