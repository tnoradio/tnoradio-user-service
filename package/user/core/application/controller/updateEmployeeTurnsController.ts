import e from "express";
import { EmployeeTurnsUpdater } from "../useCases/EmployeeTurnsUpdater";

interface UseCase {
  employeeTurnsUpdater: EmployeeTurnsUpdater;
}

export class UpdateEmployeeTurnsController {
  constructor(private useCase: UseCase) {}

  async handle(req, res) {
    try {
      //Set id
      this.useCase.employeeTurnsUpdater.setUpdateValues(req.body);
      const response = await this.useCase.employeeTurnsUpdater.execute();

      if (response == null)
        return res.status(400).send("EmployeeTurn not Updated");
      else return res.status(200).send(response);
    } catch (err) {
      return res.status(400).send(err);
    }
  }
}
