import e from 'express';
import { ImageGetter } from '../useCases/ImageGetter';

export interface UseCase {
    getUserImage: ImageGetter;
}

export class GetUserImageController {

    constructor(private useCase: UseCase) {  }

    async handle(req: e.Request, res: e.Response) {
        //console.log("SHOW IMAGE CONTROLLER");
        console.log(req.params);

        try {

            var options = {
                root: 'public/user_images/' + req.params.type,
                dotfiles: 'deny',
                headers: {
                  'x-timestamp': Date.now(),
                  'x-sent': true
            }}

            this.useCase.getUserImage.setImageName(req.params.name);
            this.useCase.getUserImage.setOptions(options);
            this.useCase.getUserImage.setResponse(res);      

            const response = this.useCase.getUserImage.execute();

            return response;

        } catch (error) {
            console.log(error);
            return res.status(400).send(error);
        }
    }
}