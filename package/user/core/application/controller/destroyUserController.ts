import e from 'express';
import { UserDestory } from '../useCases/UserDestroy';


interface UseCase {
    destroyUser: UserDestory;
}

export class DestroyUserController {

    constructor(private useCase: UseCase) {
    }

    async handle(req: e.Request, res: e.Response) {
        

        this.useCase.destroyUser.setUserId(req.params._id);

        try {
            if (this.useCase.destroyUser.getUserId != null) {
                //console.log(req.params._id);
                const data = await this.useCase.destroyUser.execute();
                return res.status(200).send(data);
            } else {
                return res.status(400).send("ID not found");
            }
        } catch (err) {
            return res.status(400).send(err);
        }


    }
}