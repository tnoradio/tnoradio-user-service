import e from "express";
import { EmployeeTurnsReader } from "../useCases/EmployeeTurnsReader";

interface UseCases {
  getEmployeeTurnsById: EmployeeTurnsReader;
}

export class GetEmployeeTurnsByIdController {
  constructor(private usescases: UseCases) {}
  async handle(req: e.Request, res: e.Response) {
    //definition to bring all users
    this.usescases.getEmployeeTurnsById.setId(req.params._id);

    try {
      const data = await this.usescases.getEmployeeTurnsById.execute();
      if (data == null) return res.status(400).send("EmployeeTurns not Found");
      else return res.status(200).send(data);
    } catch (err) {
      return res.status(400).send(err);
    }
  }
}
