import e from "express";
import { NotificationCreator } from "../useCases/NotificationCreator";

interface UsesCase {
  createNotification: NotificationCreator;
}

export class CreateNotificationController {
  constructor(private useCase: UsesCase) {}

  async handle(req: e.Request, res: e.Response) {
    this.useCase.createNotification.setNotificationCreate(req.body);

    try {
      const notification = await this.useCase.createNotification.execute();
      console.log(notification);
      return res.status(201).send(notification);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
