import e from "express";
import { MailSender } from "../useCases/MailSender";

interface UseCase {
  sendEmail: MailSender;
}

export class SendEmailController {
  constructor(private useCase: UseCase) {}

  async handle(req: e.Request, res: e.Response) {
    console.log("CONTROLLER");
    console.log(req.params);
    try {
      this.useCase.sendEmail.setUserEmail(req.params.email);
      this.useCase.sendEmail.setEmailText(req.params.text);
      this.useCase.sendEmail.setSubject(req.params.subject);

      const response = await this.useCase.sendEmail.execute();

      return res.status(201).send(response);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
