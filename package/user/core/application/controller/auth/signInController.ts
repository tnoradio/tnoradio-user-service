import e from 'express';
import { SignIn } from '../../useCases/auth/SignIn';

interface UsesCases {
    getUserLogin: SignIn;
}

export class SignInController {

    constructor(private usescases: UsesCases) { }

    async handle(req: e.Request, res: e.Response) {

        try {
            this.usescases.getUserLogin.userLoginParams(req.body)

            const data = await this.usescases.getUserLogin.execute();

            if(data instanceof Error) {
                console.log("ERROR DTA ", data.message);
                return res.status(400).send(data.message);
            }
            else res.status(200).send(data);

        } catch (err) {
            console.log(err);
            return res.status(400).send({ data: err });
        }
    }
}