import e from 'express';
import { SignUp } from '../../useCases/auth/SignUp';


interface UsesCases {
    signup: SignUp;
}

export class SignUpController {

    constructor(private usescases: UsesCases) {
    }

    async handle(req: e.Request, res: e.Response) {

        this.usescases.signup.UserCreate(req.body);

        let isTaken = await this.usescases.signup.emailIsTaken(req.body.email);

        //isTaken = false;

        if (!isTaken) {
            let verifyMatch = this.usescases.signup
                .validateMatchPassword(this.usescases.signup.getUserCreate().password,
                    this.usescases.signup.getUserCreate().passwordConfirmation)

            if (verifyMatch) {

                try {
                    const data = await this.usescases.signup.execute();
                    return res.status(201).send({ user_data: data });
                } catch (err) {
                    console.log(err);
                    return res.status(400).send({ data: err });
                }

            }
            else {
                const data = {
                    error: "Passwords don't match",
                    status: 400
                };
                return res.status(data.status).send({ error: data });
            }
        } else {
            const data = {
                error: "Email is Taken",
                status: 400
            };
            console.log(data);
            return res.status(data.status).send({ error: data });
        }

    }
}