import e from "express";
import { NotificationUpdateStatus } from "../useCases/NotificationUpdateStatus";

interface UseCase {
  updateNotificationStatus: NotificationUpdateStatus;
}

export class UpdateNotificationStatusController {
  constructor(private useCase: UseCase) {}

  async handle(req, res) {
    console.log("UPDATE");
    console.log(req.params);
    console.log(req.body);
    try {
      //Set id
      this.useCase.updateNotificationStatus.setId(req.body._id);
      this.useCase.updateNotificationStatus.setStatus(req.body.isActive);

      const response = await this.useCase.updateNotificationStatus.execute();

      console.log("Notification CONTROLLER", response);

      if (response == null)
        return res.status(400).send("Notification not Updated");
      else return res.status(200).send(response);
    } catch (err) {
      return res.status(400).send(err);
    }

    /*if (isDomainError(product)) {
            res.status(400).send(product.message);
        } else {
            res.send(product);
        }*/
  }
}
