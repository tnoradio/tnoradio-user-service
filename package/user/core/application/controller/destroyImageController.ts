import e from 'express';
import { DestroyImage } from '../useCases/ImageDeleter';

interface UseCase {
    destroyImage: DestroyImage;
}

export class DeleteUserController {

    constructor(private useCase: UseCase) { }

    async handle(req: e.Request, res: e.Response) {
        console.log("CONTROLLER " , req.params);
        try {
            //Set id 
            this.useCase.destroyImage.setImageUrl(req.params.imageUrl);

            const data = await this.useCase.destroyImage.execute();
            console.log("DELETE ", data);
            if (data == null)
                return res.status(400).send("Image not Deleted");
            else
                return res.status(200).send(data);
        } catch (err) {
            return res.status(400).send(err);
        }

        /*if (isDomainError(product)) {
            res.status(400).send(product.message);
        } else {
            res.send(product);
        }*/
    }
}