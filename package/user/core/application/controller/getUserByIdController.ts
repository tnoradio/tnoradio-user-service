import e from "express";
import { UserByIdGetter } from "../useCases/UserByIdGetter";

interface UseCases {
  getUserById: UserByIdGetter;
}

export class GetUserByIdController {
  constructor(private usescases: UseCases) {}

  async handle(req: e.Request, res: e.Response) {
    //definition to bring all users
    this.usescases.getUserById.createUserId(req.params._id);

    try {
      const data = await this.usescases.getUserById.execute();
      //console.log("CONTRLLER",data);
      if (data == null) return res.status(400).send("User not Found");
      else return res.status(200).send(data);
    } catch (err) {
      return res.status(400).send(err);
    }

    /*if (isDomainError(product)) {
            res.status(400).send(product.message);
        } else {
            res.send(product);
        }*/
  }
}
