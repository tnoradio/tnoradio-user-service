import e from "express";
import { EmployeeTurnCreator } from "../useCases/EmployeeTurnCreator";

interface UsesCase {
  createEmployeeTurn: EmployeeTurnCreator;
}

export class CreateEmployeeTurnController {
  constructor(private useCase: UsesCase) {}

  async handle(req: e.Request, res: e.Response) {
    console.log("CONTROLLER");
    console.log(req.body);

    this.useCase.createEmployeeTurn.setEmployeeTurn(req.body);

    try {
      const employeeTurn = await this.useCase.createEmployeeTurn.execute();
      console.log(employeeTurn);
      return res.status(201).send(employeeTurn);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
