import e from "express";
import { UserUpdate } from "../useCases/UserUpdate";

interface UseCase {
  updateUser: UserUpdate;
}

export class UpdateUserController {
  constructor(private useCase: UseCase) {}

  async handle(req, res) {
    try {
      this.useCase.updateUser.setId(req.params._id);
      this.useCase.updateUser.setDomainUser(req.body);
      const user = await this.useCase.updateUser.execute();

      if (user == null) return res.status(400).send("User not Updated");
      else return res.status(200).send(user);
    } catch (err) {
      return res.status(400).send(err);
    }
  }
}
