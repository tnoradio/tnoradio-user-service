import e from "express";
import { UserBySlugGetter } from "../useCases/UserBySlugGetter";

interface UseCase {
  getUserBySlug: UserBySlugGetter;
}

export class GetUserBySlugController {
  constructor(private useCase: UseCase) {}

  async handle(req: e.Request, res: e.Response) {
    //definition to bring all users
    this.useCase.getUserBySlug.createUserSlug(req.params.slug);

    try {
      const data = await this.useCase.getUserBySlug.execute();
      //console.log("CONTRLLER",data);
      if (data == null) return res.status(400).send("User not Found");
      else return res.status(200).send(data);
    } catch (err) {
      return res.status(400).send(err);
    }
  }
}
