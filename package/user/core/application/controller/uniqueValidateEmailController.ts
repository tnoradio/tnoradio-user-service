import e from 'express';
import { UserVerifyEmail } from '../useCases/UniqueEmailValidator';

interface UsesCases {
    getVerifyEmail: UserVerifyEmail;
}

export class UniqueValidateController {

    constructor(private usescases: UsesCases) { }

    async handle(req: e.Request, res: e.Response) {

        try {
            this.usescases.getVerifyEmail.setEmail(req.body.email)

            const data = await this.usescases.getVerifyEmail.execute();

            return res.status(200).send(data);
        } catch (err) {
            return res.status(400).send(err);
        }
    }
}