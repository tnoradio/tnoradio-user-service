import e from 'express';
import { UserCreator } from '../useCases/UserCreator';


interface UsesCase {
    createUser: UserCreator;
}

export class CreateUserController {

    constructor(private useCase: UsesCase) { }

    async handle(req: e.Request, res: e.Response) {

        console.log("CONTROLLER");
        console.log(req.body);

        this.useCase.createUser.UserCreate(req.body);

        let isTaken = await this.useCase.createUser.emailIsTaken(req.body.email);

        //isTaken = false;

        if (!isTaken) {
            let verifyMatch = this.useCase.createUser
                .validateMatchPassword(this.useCase.createUser.getUserCreate().password,
                    this.useCase.createUser.getUserCreate().passwordConfirmation)

            if (verifyMatch) {

                try {
                    const user = await this.useCase.createUser.execute();
                    console.log(user);
                    return res.status(201).send(user);
                } catch (error) {
                    console.log(error);
                    return res.status(400).send(error);
                }
            }
            else {
                const error = {
                    error: "Passwords don't match",
                    status: 400
                };
                return res.status(error.status).send(error);
            }
        } else {
            const error = {
                error: "Email is Taken",
                status: 400
            };
            console.log("CONTROLLER 50 ", error);
            return res.status(error.status).send(error);
        }
    }
}