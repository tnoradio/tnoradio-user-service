import e from 'express';
import { RoleCreator } from '../useCases/RoleCreator';


interface UseCase {
    createRole: RoleCreator;
}

export class CreateRoleController {

    constructor(private useCase: UseCase) {
    }

    async handle(req: e.Request, res: e.Response) {

        this.useCase.createRole.setRole(req.body);       

        try {
            const role = await this.useCase.createRole.execute();
            return res.status(201).send(role);
                   
        } catch (error) {
            console.log(error);
            return res.status(400).send(error);
        }

    }
}