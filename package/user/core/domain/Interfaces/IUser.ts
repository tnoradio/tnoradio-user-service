import UserImage from "../entities/UserImage";
import { IUserEmail } from "./IUserEmail";
import { IUserRole } from "./IUserRole";
import { IUserSocial } from "./IUserSocial";
import { IEmployeeTurn } from "./IEmployeeTurn";

export interface IUser {
  _id: String;
  user_id: String;
  name: String;
  lastName: String;
  bio: String;
  birthdate: Date;
  image: String;
  DNI: Number;
  email: IUserEmail;
  socials: IUserSocial[];
  isActive: Boolean;
  isWorking: Boolean;
  password: String;
  passwordConfirmation: String;
  gender: String;
  images: UserImage[];
  roles: IUserRole[];
  address: String;
  notes: String;
  company: String;
  department: String;
  phone: String;
  isStarred: Boolean;
  isDeleted: Boolean;
  updatedBy: String;
  slug: String;
  turns: IEmployeeTurn[];
}
