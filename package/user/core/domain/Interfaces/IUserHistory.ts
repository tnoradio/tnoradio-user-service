import { IUserEmail } from "./IUserEmail";

export interface IUserHistory {
  _id: String;
  user_id: String;
  name: String;
  lastName: String;
  image: String;
  DNI: Number;
  email: IUserEmail;
  isActive: Boolean;
  isWorking: Boolean;
  password: String;
  passwordConfirmation: String;
  gender: String;
  role: String;
  address: String;
  notes: String;
  company: String;
  department: String;
  phone: String;
  isStarred: Boolean;
  isDeleted: Boolean;
  updatedBy: String;
}
