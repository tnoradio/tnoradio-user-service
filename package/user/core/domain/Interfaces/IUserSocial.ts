
export interface IUserSocial {
    _id: String;
    socialNetwork: String;
    url: String;
    userName: String;
}
