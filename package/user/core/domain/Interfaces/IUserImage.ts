
export interface IUserImage {
    _id: String;
    imageName: String;
    imageUrl: String;
    file:
        {
            data: Buffer,
            contentType: String
        }
    owner: String;
}
