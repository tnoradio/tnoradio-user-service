import { IEmployeeActivity } from "./IEmployeeActivity";

export interface IEmployeeTurn {
  _id: String;
  startTime: Date; // Attribute just for user_history
  endTime: Date;
  owner: String;
  date: Date;
  activities: IEmployeeActivity;
}
