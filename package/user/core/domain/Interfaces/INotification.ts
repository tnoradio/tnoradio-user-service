export interface INotification {
  _id: String;
  type: String;
  isActive: Boolean;
  message: String;
  user: String;
}
