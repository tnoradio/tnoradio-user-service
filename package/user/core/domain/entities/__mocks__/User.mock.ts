import DomainUser from "../User"
import UserEmail from "../UserEmail"

/**
 * Manual Moks
 */
const email = new UserEmail('michaeljordan@nba.com')
const userAudienceMock =
    new DomainUser(
        '123456789',
        'Michael',
        'Jordan',
        'bio',
        'foto_perfil.jpg',
        1111,
        email,
        true,
        [],
        "1111",
        "1111",
        'M',
        [],
        [],
        'USA',   
        'King of NBA',
        'NBA',
        'SPORTS',
        '34424234234',
        false,
        false,
        '987654321',
        'michaeljordan'
    );


const emailAdmin = new UserEmail('jessica@gmail.com')
const userAdminMock = new DomainUser(
    '123456789',
        'Michael',
        'Jordan',
        'bio',
        'foto_perfil.jpg',
        1111,
        email,
        true,
        [],
        "1111",
        "1111",
        'M',
        [],
        [],
        'USA',   
        'King of NBA',
        'NBA',
        'SPORTS',
        '34424234234',
        false,
        false,
        '987654321',
        'michaeljordan'
);

export default {
    userAudienceMock,
    userAdminMock
}
