export default class UserRole {
    _id: String;
    role: String;

    constructor(
        _id: String, role: String) {
        this._id = _id;
        this.role = role;
    }


    static create(
        _id: String, role: String) {

        return new UserRole(_id, role);

    }

}  