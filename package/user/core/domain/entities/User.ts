import EmployeeTurn from "./EmployeeTurn";
import UserEmail from "./UserEmail";
import UserImage from "./UserImage";
import UserRole from "./UserRole";
import UserSocial from "./UserSocial";

export default class DomainUser {
  _id: String;
  user_id: String; // Attribute just for user_history
  name: String;
  lastName: String;
  birthdate: Date;
  bio: String;
  image: String;
  DNI: Number;
  socials: UserSocial[];
  images: UserImage[];
  email: UserEmail;
  isActive: Boolean;
  isWorking: Boolean;
  password: String;
  passwordConfirmation: String;
  gender: String;
  roles: UserRole[];
  address: String;
  notes: String;
  company: String;
  department: String;
  phone: String;
  isStarred: Boolean;
  isDeleted: Boolean;
  updatedBy: String;
  slug: String;
  turns: EmployeeTurn[];

  constructor(
    _id: String,
    name: String,
    lastName: String,
    bio: String,
    image: String,
    DNI: Number,
    email: UserEmail,
    isActive: Boolean,
    isWorking: Boolean,
    socials: UserSocial[],
    password: String,
    passwordConfirmation: String,
    gender: String,
    images: UserImage[],
    roles: UserRole[],
    address: String,
    notes: String,
    company: String,
    department: String,
    phone: String,
    isStarred: Boolean,
    isDeleted: Boolean,
    updatedBy: String,
    slug: String,
    user_id?: String,
    birthdate?: Date,
    turns?: EmployeeTurn[]
  ) {
    this._id = _id;
    this.name = name;
    this.lastName = lastName;
    this.bio = bio;
    this.image = image;
    this.DNI = DNI;
    this.socials = socials;
    this.email = email;
    this.isActive = isActive;
    this.isWorking = isWorking;
    this.password = password;
    this.passwordConfirmation = passwordConfirmation;
    this.gender = gender;
    this.images = images;
    this.roles = roles;
    this.address = address;
    this.notes = notes;
    this.company = company;
    this.department = department;
    this.phone = phone;
    this.isStarred = isStarred;
    this.isDeleted = isDeleted;
    this.updatedBy = updatedBy;
    this.user_id = user_id;
    this.slug = slug;
    this.birthdate = birthdate;
    this.turns = turns;
  }

  static create(
    _id: String,
    name: String,
    lastName: String,
    bio: String,
    image: String,
    DNI: Number,
    email: UserEmail,
    isActive: Boolean,
    isWorking: Boolean,
    socials: UserSocial[],
    password: String,
    passwordConfirmation: String,
    gender: String,
    images: UserImage[],
    roles: UserRole[],
    address: String,
    notes: String,
    company: String,
    department: String,
    phone: String,
    isStarred: Boolean,
    isDeleted: Boolean,
    updatedBy: String,
    slug: String,
    user_id?: String,
    birthdate?: Date,
    turns?: EmployeeTurn[]
  ) {
    return new DomainUser(
      _id,
      name,
      lastName,
      bio,
      image,
      DNI,
      email,
      isActive,
      isWorking,
      socials,
      password,
      passwordConfirmation,
      gender,
      images,
      roles,
      address,
      notes,
      company,
      department,
      phone,
      isStarred,
      isDeleted,
      updatedBy,
      slug,
      user_id,
      birthdate,
      turns
    );
  }
}
