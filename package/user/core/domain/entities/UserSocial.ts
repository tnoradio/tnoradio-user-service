export default class UserSocial {
    _id: String;
    socialNetwork: String;
    url: String;
    userName: String;

    constructor(
        _id: String, socialNetwork: String, url: String, userName: String) {
        this._id = _id;
        this.socialNetwork = socialNetwork;
        this.url = url;
        this.userName =  userName;
    }

    static create(
        _id: String, socialNetwork: String, url: String, userName: String) {

        return new UserSocial(_id, socialNetwork, url, userName);
    }
}  