import { StringValueObject } from '../value-object/String.vo'
import { InvalidArgumentError } from '../value-object/InvalidArgumentError'

export default class UserEmail extends StringValueObject {
  constructor(value: String) {
    super(value);
    this.isValidEmail(value);
  }

  isValidEmail(value: String): void {
    const regExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (regExp.test(String(value).toLowerCase()) == false) {
      throw new InvalidArgumentError(`El Email <${value}> no tiene el formato correcto`);
    }
  }
}