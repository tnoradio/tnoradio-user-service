export default class EmployeeActivity {
  _id: String;
  duration: Date; // 
  name: String;
  taskId: String;
  turnId: String;

  constructor(
    _id: String,
    duration: Date, // 
    name: String,
    taskId: String,
    turnId: String,
  ) {
    this._id = _id;
    this.duration = duration;
    this.name = name;
    this.taskId = taskId;
    this.turnId = turnId;
  }

  static create(
    _id: String,
    duration: Date, // 
    name: String,
    taskId: String,
    turnId: String
  ) {
    return new EmployeeActivity(_id, duration, name, taskId, turnId);
  }
}
