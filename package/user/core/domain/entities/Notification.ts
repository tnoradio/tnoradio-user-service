export default class DomainNotification {
  _id: String;
  message: String; // Attribute just for user_history
  isActive: Boolean;
  user: String;
  type: String;

  constructor(
    _id: String,
    message: String,
    user: String,
    isActive: Boolean,
    type: String
  ) {
    this._id = _id;
    this.message = message;
    this.isActive = isActive;
    this.user = user;
    this.type = type;
  }

  static create(
    _id: String,
    message: String,
    user: String,
    isActive: Boolean,
    type: String
  ) {
    return new DomainNotification(_id, message, user, isActive, type);
  }
}
