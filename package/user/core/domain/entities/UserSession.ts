import User from "./User";

export class UserSession {
    readonly user: User;
    readonly token: String;

    constructor(user: User, token: String) {
        this.user = user;
        this.token = token;
    }

    createSession(user: User, token: String) {
        return new UserSession(user, token);
    }
}