export interface UserEmailService {
  sendAppEmail(email: String, subject: String, text: String);
}
