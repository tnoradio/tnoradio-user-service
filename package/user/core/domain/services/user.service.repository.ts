import DomainUser from "../entities/User";
import DomainNotification from "../entities/Notification";
import UserEmail from "../entities/UserEmail";
import UserImage from "../entities/UserImage";
import UserRole from "../entities/UserRole";
import EmployeeTurn from "../entities/EmployeeTurn";

export interface UserRepository {
  save(user: DomainUser): Promise<DomainUser | Error>;
  saveRole(role: UserRole): Promise<UserRole>;
  isValidEmail(email: UserEmail): Promise<Boolean>;
  getUserByEmail(email: UserEmail): Promise<DomainUser>;
  getUserById(_id: String): Promise<DomainUser>;
  getUserBySlug(_slug: String): Promise<DomainUser>;
  getAll(): Promise<DomainUser[] | Error>;
  update(
    user: DomainUser,
    id?: String,
    email?: UserEmail
  ): Promise<DomainUser | Error>;
  destroy(userId: String): Promise<any | Error>;
  destroyImage(imageUrl: String): Promise<any>;
  delete(userId: String): Promise<DomainUser | Error>;
  insertFromHistoryUser(user: DomainUser): Promise<any | Error>;
  getUserImage(name: String, slug: String): Promise<UserImage>;
  saveUserImage(image): Promise<UserImage>;
  updateUserImage(image, slug, name): Promise<UserImage>;

  // Auth
  signup(user: DomainUser): Promise<any | Error>;
  login(email: UserEmail, password: String): Promise<any | Error>;
  resetPassword(_id: String, password: String): Promise<any>;
  // Notification
  saveNotification(
    notification: DomainNotification
  ): Promise<DomainNotification | Error>;
  getAllNotificationUser(id): Promise<Error | DomainNotification[]>;
  updateStatusNotification(id, isActive): Promise<Error | DomainNotification>;

  // EmployeeTurn
  saveEmployeeTurn(employeeTurn: EmployeeTurn): Promise<EmployeeTurn>;
  getEmployeeTurns(_id): Promise<EmployeeTurn[]>;
  updateEmployeeTurn(updateValues): Promise<EmployeeTurn>;
  destroyEmployeeTurn(employeeTurnId: String): Promise<any>;
}
