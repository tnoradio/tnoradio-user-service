export interface UserImageRepository {
    getUserImage(imageName, options, res) : any;
}