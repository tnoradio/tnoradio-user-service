
import config from "../config";
const kafkaLogging = require('kafka-node/logging');

function consoleLoggerProvider (name) {
  // do something with the name
  console.log("NAME CONSOLE LOGGER DEG", name)
  return {
    debug: console.debug.bind(console),
    info: console.info.bind(console),
    warn: console.warn.bind(console),
    error: console.error.bind(console)
  };
}

kafkaLogging.setLoggerProvider(consoleLoggerProvider);
import kafka from "kafka-node";

const client = new kafka.KafkaClient({ kafkaHost: config.kafka_host });

var showConsumer = new kafka.Consumer(client, [{ topic: "shows1" }], {fetchMaxBytes: 1024 * 1024});

var producer = new kafka.Producer(client);

client.on("error", (e) => {
  console.log("Error arrancando client");
  console.log(e);
});
client.on("ready", () => {
  console.log("Kafka client ready");
});

showConsumer.on("error", (e) => {
  console.log("Error arrancando consumer");
  console.log(e);
});

showConsumer.on("ready", () => {
  console.log("Kafka consumer is ready");
});

producer.on("error", (e) => {
  console.log("Error arrancando producer");
  console.log(e);
});

producer.on("ready", () => {
  console.log("Kafka producer is ready");
  producer
    .send([{topic: "users", messages: "Users producer test"}],
      (err, data) => {
        console.log("Error in users producer send test ", err);
        console.log("Data in users producer send test ", data);
      });
});

export default {
  showConsumer: showConsumer,
  producer: producer,
};
