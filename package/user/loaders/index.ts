import expressLoader from "./express";
import mongooseLoader from "./mongoose";
import express from "express";
import config from "../config";

export const app = express();
//Adds Express Static Middleware
app.use(express.static("public"));


export default async () => {
  /**
   * Port loader
   */
  await app.listen(config.port || 5001, () => {
    try {
      console.info(`
        ################################################
        🛡️  Server listening on port: ${config.port} 🛡️ 
        ################################################
      `);
    } catch (err) {
      console.error(err);
      process.exit(1);
    }
  });

  /**
   * MongoDB loader, creates mongoClient and connect to the db and return db connection.
   */
  const mongoConnection = await mongooseLoader();
  console.info("✌️ DB loaded and connected!");

  /**
   * Laods express essentials
   */
  await expressLoader({ app });
  console.log("info", "Express Loader has initalized successfully! ✅");
};
