# END POINTS

### NOTIFICACIONES

| Tipo | Path   | DESCRIPCION |
| ------------- | ------------- |------------- |
| POST | /api/notifications/save | formato json: { "user": <"id del usuario - String">, "message": <"mensaje - String">,"isActive": <"estatus de la notificacion - Boolean">,"type": <"tipo de notificacion - String">} |
| GET | /api/notifications/getbyid/:_id | _id: Id del Usuario |
| PATCH | /api/notifications/updatestatus/:_id | _id: Id de la notificación
