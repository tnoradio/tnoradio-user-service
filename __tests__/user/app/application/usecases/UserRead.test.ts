import { UsersRead } from '../../../../../package/user/app/application/usecases/UsersRead'
jest.mock('../../../../../package/user/app/application/usecases/UsersRead')

import { UserMongoRepository } from '../../../../../package/user/app/infrastructure/mongo/user.mongo.repository';
jest.mock('../../../../../package/user/app/infrastructure/mongo/user.mongo.repository');

//TS needs function typeof<SomeClass> to create mock
const _usersRead = <jest.Mock<UsersRead>>UsersRead;
const _userMongoRepository = <jest.Mock<UserMongoRepository>>UserMongoRepository;

const useCaseUserRead = new _usersRead(new _userMongoRepository())


describe('Use Case : Users Read', () => {

    beforeEach(() => {
        // Clear all instances and calls to constructor and all methods:
        _usersRead.mockClear();
    });

    it('Check if the consumer called the class constructor', () => {
        let called = new _usersRead(new _userMongoRepository());
        expect(_usersRead).toHaveBeenCalledTimes(1);
    });

    test('Set Type read use case', () => {
        expect(useCaseUserRead.getTypeReading()).not.toBeNull();
        expect(useCaseUserRead.getTypeReading()).toBeUndefined();
    });

})