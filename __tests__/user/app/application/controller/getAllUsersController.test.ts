import { UsersRead } from '../../../../../package/user/app/application/usecases/UsersRead'
jest.mock('../../../../../package/user/app/application/usecases/UsersRead')

import { GetAllUsersController } from '../../../../../package/user/app/application/controller/getAllUsersController'
jest.mock('../../../../../package/user/app/application/controller/getAllUsersController')

import mock from '../../../../../package/user/app/application/controller/__mock__/getAllUsers.request.mock';
import { Express } from 'express';

const _usersRead = <jest.Mock<UsersRead>>UsersRead;
const _getAllUsersController = <jest.Mock<GetAllUsersController>>GetAllUsersController;

describe('Controller : Users Read', () => {

    beforeEach(() => {
        let called = new _getAllUsersController(_usersRead);
    });

    test('Called to constructor times', () => {
        expect(_getAllUsersController).toHaveBeenCalledTimes(1);
    });

   /* test('Async handler function', () => {
        let controller = new _getAllUsersController(_usersRead);
        let status = controller.handle(mock.mockRequest, mock.mockResponse);
        expect(status).not.toBeUndefined();

    });*/


})