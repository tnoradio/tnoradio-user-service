FROM node:12
RUN mkdir -p /usr/src
WORKDIR /usr/src
COPY package.json /usr/src
RUN npm install 
COPY . /usr/src
EXPOSE 5001
CMD npm start